/* 
 * Copyright (c) 2012-2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
/*
 *  ======== WatchdogTimer.xs ========
 *
 */

var Exception = null;
var Clock = null;

/*
 *  ======== module$meta$init ========
 */
function module$meta$init()
{
    /* Only process during "cfg" phase */
    if (xdc.om.$name != "cfg") {
        return;
    }
    
    this.RSTCTRL = 0x023100E8;
    this.RSTCFG  = 0x023100EC;
    this.RSTMUXnBASE = 0x02620308;
    this.TINTLDSPINT = 64;

    if (Program.cpu.deviceName.match(/6614/)) {
        /* Appleton */
        this.biosReplacementTimer = 4;
        this.biosReplacementTimerOwner = 0;
    }
    else {
        /* K2 devices */
        if (Program.cpu.deviceName.match(/6636/) ||
            Program.cpu.deviceName.match(/6638/)) {
            /* K2H & K2K */
            this.biosReplacementTimer = 8;
            this.biosReplacementTimerOwner = 0; 
        }
        else if (Program.cpu.deviceName.match(/6630/)) {
            /* K2L */
            this.biosReplacementTimer = 8;
            this.biosReplacementTimerOwner = 0;            
        }
        else if (Program.cpu.deviceName.match(/AK2E/)) {
            /* K2E */
            this.biosReplacementTimer = 8;
            this.biosReplacementTimerOwner = 0;            
        }
    }

    /* RegOverlay gets mapped in the source Init function since its address is based on
     * the DSP number */
}

/*
 *  ======== module$use ========
 */
function module$use()
{
    var Clock = xdc.useModule('ti.sysbios.knl.Clock');
    var Timer = xdc.useModule('ti.sysbios.timers.timer64.Timer');

    WatchdogTimer = this;

    /* Only configure the timerId if user has not done so in their *.cfg file */
    if (!(Clock.$written("timerId"))) {
        /* BIOS must be configured to use a different timer for the Clock module
         * since the local timers for each core are the watchdog timers */
        Clock.timerId = WatchdogTimer.biosReplacementTimer;          // This will set BIOS to use a different timer for the Clock module
        
        /* The timer will be shared by all cores.  Configure the owner core */
        Timer.timerSettings[Clock.timerId].ownerCoreId = WatchdogTimer.biosReplacementTimerOwner;
    }    
}

/*
 *  ======== module$static$init ========
 */
function module$static$init(mod, params)
{
    mod.wdTimerState = this.wdStates_INITIAL;
}
