/* 
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
/*
 *  ======== WatchdogTimer.xdc ========
 *
 */

module WatchdogTimer
{
    // -------- External Module Constants --------

    /*
     *  Configuration values for Reset Configuration Register (RSTCFG).  Enum values based on register field values.
     *  @p(blist)
     * -HARD_RESET -> Watchdog timer reset will be a hard reset
     * -LOCAL_RESET_INPUT -> Watchdog timer reset will be a soft reset
     *  @p     
     */
    enum RstcfgResetType {
        RstcfgResetType_HARD_RESET = 0,
        RstcfgResetType_SOFT_RESET = 1
    };

     // -------- Module API Structures --------

    /*!     
     *  Output modes for the RSTMUX register which decides how the system handles Watchdog timer timeouts.  Output modes
     *  from "Reset Mux Register Field Descriptions Table" (Ex, Table 3-19 of SPRS671B for 6614) of device Data manual
     *  @p(blist)
     * -DEFAULT_NO_OUTPUT_EVENT -> Default on system reset, WD timeout causes no output event
     * -LOCAL_RESET_INPUT -> WD timeout causes local reset input to CorePac
     * -NMI_INPUT -> WD timeout causes NMI input to CorePac
     * -NMI_INPUT_PLUS_LOCAL_RESET_INPUT -> WD timeout causes an NMI input followed by a local reset input.  The delay between
     *                                                                   the NMI and the local reset is defined by RstmuxDelay configuration parameter
     * -DEVICE_RESET -> WD timeout event causes a device reset
     *  @p
     */ 
    enum RstmuxOmode {
        RstmuxOmode_DEFAULT_NO_OUTPUT_EVENT = 0,
        RstmuxOmode_LOCAL_RESET_INPUT = 2,
        RstmuxOmode_NMI_INPUT = 3,
        RstmuxOmode_NMI_INPUT_PLUS_LOCAL_RESET_INPUT = 4,
        RstmuxOmode_DEVICE_RESET = 5
    };

    /*!     
     *  Delay for the RSTMUX register which decide how long the system delays between NMI and local reset when there's a
     *  watchdog timeout.  Only valid when RstmuxOmode_NMI_LOCAL_RESET is used.
     */ 
    enum RstmuxDelay {
        RstmuxDelay_256CPU_DIV_6_CYCLES = 0,
        RstmuxDelay_512CPU_DIV_6_CYCLES = 1,
        RstmuxDelay_1024CPU_DIV_6_CYCLES = 2,
        RstmuxDelay_2048CPU_DIV_6_CYCLES = 3,
        RstmuxDelay_4096CPU_DIV_6_CYCLES_DEFAULT = 4,
        RstmuxDelay_8192CPU_DIV_6_CYCLES = 5,
        RstmuxDelay_16384CPU_DIV_6_CYCLES = 6,
        RstmuxDelay_32768CPU_DIV_6_CYCLES = 7
    };

    /*!
     *  InitCfg - Watchdog Timer initialization parameters
     *
     *  @p(blist)
     * -wdPeriodLo -> Lower 32 bits of the watchdog timer period.  The 64-bit period specifies the number of clock ticks
     *                         prior to the watchdog timer timeout.
     * -wdPeriodHi -> Upper 32 bits of the watchdog timer period.  The 64-bit period specifies the number of clock ticks
     *                         prior to the watchdog timer timeout.
     * -wdResetType -> Specifies the reset type if watchdog timer is configured to reset DSP on timeout this parameter.
     * -wdRegTimeoutExc -> Specifies whether a watchdog timeout should be handled as an exception through the BIOS
     *                                   Exception module
     * -rstOmode -> Specifies the output mode of a watchdog timer timeout
     * -rstDelay -> Specifies the delay between the NMI and reset if the NMI_INPUT_PLUS_LOCAL_RESET_INPUT rstOmode
     *                     is selected.
     *  @p
     */ 
    struct InitCfg {
        Bits32 wdPeriodLo;
        Bits32 wdPeriodHi;
        RstcfgResetType wdResetType;
        Bool wdRegisterTimeoutExc; 
        RstmuxOmode rstOmode;
        RstmuxDelay rstDelay;
    };

    /*!     
     *  Status return codes
     *  @p(blist)
     * -OKAY -> No error
     * -ERROR_INVALID_RSTCFG_VAL -> Invalid value specified for Watchdog timer reset type
     * -ERROR_ALREADY_IN_ACTIVE_STATE -> When trying to move timer to active state when it's already in this state
     * -ERROR_ALREADY_IN_SERVICE_STATE -> When trying to move timer to service state when it's already in this state
     *  @p
     */ 
    enum Status {
        Status_OKAY = 0,
        Status_ERROR_INVALID_RSTCFG_VAL = 1,
        Status_ERROR_ALREADY_IN_ACTIVE_STATE = 2,
        Status_ERROR_ALREADY_IN_SERVICE_STATE = 3
    };

    /*!
     *  ======== biosReplacementTimer ========
     *  Timer used by BIOS in place of local timer that is used for watchdog timer
     */
    config UInt biosReplacementTimer = 0;

    /*!
     *  ======== biosReplacementTimerOwner ========
     *  Owner of timer used by BIOS in place of local timer that is used for watchdog timer.
     *  The replacement timer is a global timer so it is used by all cores.  The owner core must be specified.
     */
    config UInt biosReplacementTimerOwner = 0;    

     // -------- Module API Functions --------

     /*
       *  DSP Watchdog Timer module initialization function.
       */
    @DirectCall
    Status Init(InitCfg *wdCfg);

     /*
       *  Fully services the watchdog timer, resetting the count back to zero.
       */
    @DirectCall
    Status ResetTimer();

     /*
       *  Moves the watchdog timer to the Service state.  This is half of the process required to reset the timer.
       *  A call to this API must be followed by a call to the MoveToActiveState() API to fully reset the timer.
       */
    @DirectCall
    Status MoveToServiceState();

     /*
       *  Moves the watchdog timer to the Active state.  This is half of the process required to reset the timer.
       *  A call to this API must have been preceded by a call to the MoveToServiceState() API to fully reset the timer.
       */
    @DirectCall
    Status MoveToActiveState();

     /*
       *  Forces a timeout of the watchdog timer.
       */
    @DirectCall
    Void ForceTimeout();    

     /*
       *  Moves DSP Watchdog timer from active state to service state
       */
    @DirectCall
    Bool HasTimeoutOccurred();    

internal:    

    // -------- Internal Module Constants --------

    /*
     *  Bitmasks and values for Reset Control Register (RSTCTRL).
     */
    const Bits32 RSTCTRL_KEY_MASK = 0x0000FFFF;
    const Bits32 RSTCTRL_KEY_SHIFT = 0x00000000;
    const Bits32 RSTCTRL_KEY_VALUE = 0x00005A69;

    /*
     *  Bitmasks and values for Reset Configuration Register (RSTCFG).
     */
    const Bits32 RSTCFG_WDTYPE_MASK = 0x00000001;

    /*
     *  Bitmasks and values for Reset Mux Register (RSTMUXn).
     */
    const Bits32 RSTMUXn_OMODE_MASK = 0x00000007;
    const Bits32 RSTMUXn_OMODE_SHIFT = 1;
    const Bits32 RSTMUXn_DELAY_MASK = 0x00000007;
    const Bits32 RSTMUXn_DELAY_SHIFT = 5;

    /*
     *  Bitmasks and values for Timer Interrupt Control and Status Register (INTCTRLSTAT).
     */
    const Bits32 INTCTRLSTAT_PRDINTEN_MASK = 0x00000001;
    const Bits32 INTCTRLSTAT_PRDINTEN_LO_SHIFT = 0;
    const Bits32 INTCTRLSTAT_PRDINTEN_HI_SHIFT = 16; 
    const Bits32 INTCTRLSTAT_PRDINTEN_ENABLE_INT = 1;  /* Enable interrupt when watchdog is enabled */

    /*
     *  Bitmasks and values for Timer Global Control Register (TGCR).
     */
    const Bits32 TGCR_TIMLORS_MASK = 0x00000001;
    const Bits32 TGCR_TIMLORS_SHIFT = 0;
    const Bits32 TGCR_TIMLORS_NOT_IN_RESET = 1;
    const Bits32 TGCR_TIMHIRS_MASK = 0x00000001;
    const Bits32 TGCR_TIMHIRS_SHIFT = 1;
    const Bits32 TGCR_TIMHIRS_NOT_IN_RESET = 1;
    const Bits32 TGCR_TIMMODE_MASK = 0x00000003;
    const Bits32 TGCR_TIMMODE_SHIFT = 2;
    const Bits32 TGCR_TIMMODE_64BIT_WD_MODE = 0x2;

    /*
     *  Bitmasks and values for Timer Control Register (TCR).
     */
    const Bits32 TCR_ENAMODE_LO_MASK = 0x00000003;
    const Bits32 TCR_ENAMODE_LO_SHIFT = 6;
    const Bits32 TCR_ENAMODE_LO_CONTINUOUS_INC = 0x2;

    /*
     *  Bitmasks and values for Watchdog Timer Control Register (WDTCR).
     */
    const Bits32 WDTCR_WDEN_MASK = 0x00000001;
    const Bits32 WDTCR_WDEN_SHIFT = 14;
    const Bits32 WDTCR_WATCHDOG_ENABLE = 1;
    const Bits32 WDTCR_WDFLAG_MASK = 0x00000001;
    const Bits32 WDTCR_WDFLAG_SHIFT = 15;
    const Bits32 WDTCR_WDKEY_MASK = 0x0000FFFF;
    const Bits32 WDTCR_WDKEY_SHIFT = 16;
    const Bits32 WDTCR_WDKEY_KEY1 = 0xA5C6;   
    const Bits32 WDTCR_WDKEY_KEY2 = 0xDA7E; 

    /*!     
     *  Watchdog Timer states from Figure 4-2 of sprugv5a - timer64P.pdf
     *  @p(blist)
     * -INITIAL -> Watchdog mode disabled
     * -PRE_ACTIVE -> Pre-active state (WDEN=1 and 0xA5C6 to WDKEY)
     * -ACTIVE -> Active state (waiting for 0xA5C6 to WDKEY)
     * -SERVICE-> Service State (still counting, waiting for 0xDA7E to WDKEY)
     *  @p
     */ 
    enum wdStates {
        wdStates_INITIAL = 0,
        wdStates_PRE_ACTIVE = 1,
        wdStates_ACTIVE = 2,
        wdStates_SERVICE = 3
    };

    /*!
     *  regOverlay - Overlay for the timer registers.
     */
    struct regOverlay {
        Bits32 RESERVED1;  // Offset 0x00
        Bits32 EMUMGT_CLKSPD;  //! Offset 0x04: Ptr to the Watchdog Timer Emulation Register
        Bits32 RESERVED2;  // Offset 0x08
        Bits32 RESERVED3;  // Offset 0x0C
        Bits32 CNTLO;  //! Offset 0x10: Ptr to the Watchdog Timer Counter Low Register 
        Bits32 CNTHI;  //! Offset 0x14: Ptr to the Watchdog Timer Counter High Register 
        Bits32 PRDLO;  //! Offset 0x18: Ptr to the Watchdog Timer Period Low Register 
        Bits32 PRDHI;  //! Offset 0x1C: Ptr to the Watchdog Timer Period High Register 
        Bits32 TCR;  //! Offset 0x20: Ptr to the Watchdog Timer Timer Control Register   
        Bits32 TGCR;  //! Offset 0x24: Ptr to the Watchdog Timer Timer Global Control Register     
        Bits32 WDTCR;  //! Offset 0x28: Ptr to the Watchdog Timer Control Register       
        Bits32 RESERVED4;  // Offset 0x2C
        Bits32 RESERVED5;  // Offset 0x30
        Bits32 RELLO;  //! Offset 0x34: Ptr to the Watchdog Timer Reload Lowl Register 
        Bits32 RELHI;  //! Offset 0x38: Ptr to the Watchdog Timer Reload High Register 
        Bits32 CAPLO;  //! Offset 0x3C: Ptr to the Watchdog Timer Capture Low Register     
        Bits32 CAPHI;  //! Offset 0x40: Ptr to the Watchdog Timer Capture High Register  
        Bits32 INTCTLSTAT;  //! Offset 0x44: Ptr to the Watchdog Timer Interrupt Control and Status Register      
    };

    /*! Ptr to the Reset Control Register */
    config Ptr RSTCTRL;    

    /*! Ptr to the Reset Configuration Register */
    config Ptr RSTCFG;    

    /*! Ptr to the Base Address of the Reset MUX Registers */
    config Ptr RSTMUXnBASE;        

    /*! Timer interrupt GEM event id (varies per device) */
    config UInt TINTLDSPINT;

    struct Module_State {
        wdStates wdTimerState; /* Tracks the watchdog timer state */
    };

}
