Watchdog Timer Example





A simple example demonstrating how to configure and use the Watchdog Timer module to monitor the DSP for application corruptions and failures.





Steps to build the test application:




1. Import the CCS WatchdogTimerTestProject from instrumentation\wdtimer\test directory. (in CCSv5.1, File->Import... Select Existing CCS/CCE Eclipse Projects)



2. Clean the WatchdogTimerTestProject project, delete the Debug and Release directories, and re-build the project.  After the build is complete, WatchdogTimerTestProject.out will be generated under instrumentation\wdtimer\test\Debug (or \Release depending on the build configuration) directory.





Steps to run WatchdogTimerTestProject in CCSv5.1:




1. Be sure to set the boot mode dip switch to no boot/EMIF16 boot mode on the EVM.



2. Group all DSP cores and then connect to the group.

3. Load instrumentation\wdtimer\test\Debug\WatchdogTimerTestProject.out on all cores.



4. Run the project.  DSP exceptions should occur on all cores.

5. Parse the I/O output for the following prints:

Core n: wdExceptionFunction - The watchdog timer has expired.
Core n: Watchdog Timer Test PASSED.

The latter print should exist for all cores in the DSP group.  The ordering of the prints will be random since the prints are based on order in which the cores received the Watchdog Timer timeout exception.  If all cores in the DSP group print PASSED the Watchdog Timer test has passed.  If at least one DSP prints FAILED the test fails.