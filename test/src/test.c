/**
 *   @file  test.c
 *
 *   @brief   
 *      Test Code to test the Watchdog Timer Module
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <c6x.h>
#include <stdint.h>
#include <string.h>

/* BIOS/XDC Include Files. */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/* WatchdogTimer module includes */
#include <ti/instrumentation/wdtimer/WatchdogTimer.h>

/* API test result */
Bool apiTestPassed = FALSE;

/**********************************************************************
 ************************* Unit Test Functions ************************
 **********************************************************************/

/* Exception function that is plugged into exception module */
void wdExceptionFunction (void)
{
    System_printf ("Core %d: wdExceptionFunction - The watchdog timer has expired.\n", DNUM);

    if (apiTestPassed)
    {
        System_printf ("Core %d: Watchdog Timer Test PASSED.\n", DNUM);
    }
    else
    {
        System_printf ("Core %d: Watchdog Timer Test FAILED.\n", DNUM);
    }      
}

/* Test task which configures the watchdog timer API tests */
void testTask (void)
{
    WatchdogTimer_InitCfg wdCfg;

    /* Initialize the watchdog timer configuration structure to all zeros */
    memset ((void *)&wdCfg, 0, sizeof(WatchdogTimer_InitCfg));

    System_printf ("Core %d: Configuring Watchdog Timer for API tests.\n", DNUM);

    /* Configure the watchdog timer with a low period and no output event
     * This scenario is used to test the different watchdog reset APIs.  After 
     * testing all reset APIs the watchdog will be forcefully timed out.  The period
     * is configured to be low enough so that if none of the reset APIs work the
     * reset will occur prior to the forced timeout. */
    wdCfg.wdPeriodLo = 0x00001000;
    wdCfg.wdPeriodHi = 0x00000000;
    wdCfg.wdResetType = WatchdogTimer_RstcfgResetType_SOFT_RESET;
    wdCfg.wdRegisterTimeoutExc = TRUE;
    wdCfg.rstOmode = WatchdogTimer_RstmuxOmode_DEFAULT_NO_OUTPUT_EVENT;
    wdCfg.rstDelay = WatchdogTimer_RstmuxDelay_4096CPU_DIV_6_CYCLES_DEFAULT;

    /* Initialize and start the timer */
    WatchdogTimer_Init(&wdCfg);

    /* Test ResetTimer API */
    System_printf ("Core %d: Servicing Watchdog Timer via ResetTimer API.\n", DNUM);
    WatchdogTimer_ResetTimer();

    /* Test individual service state APIs */
    System_printf ("Core %d: Servicing Watchdog Timer via MoveTo APIs.\n", DNUM);
    WatchdogTimer_MoveToServiceState();
    WatchdogTimer_MoveToActiveState();

    /* If reached here the API tests passed.  Force the watchdog timeout */
    apiTestPassed = TRUE;
    System_printf ("Core %d: Forcing Timeout.\n", DNUM);
    WatchdogTimer_ForceTimeout();      

    /* If the force timeout API doesn't work the exception will not execute and this
     * code will be reached. */
    System_printf ("\nCore %d: ForceTimeout API Test FAILED.", DNUM);
}

int32_t main(int32_t argc, char* argv[])
{
    Task_Params         taskParams;

    System_printf ("**********************************************\n");
    System_printf ("********** Watchdog Timer Unit Test **********\n");
    System_printf ("**********************************************\n");

    /* Initialize the Task Parameters. */
    Task_Params_init(&taskParams);

    /* Create a task that causes an exception */
    Task_create((Task_FuncPtr)testTask, &taskParams, NULL);

    BIOS_start();
    return 0;
}

