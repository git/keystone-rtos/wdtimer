/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the Watchdog Timer Module
 *
 * Copyright (C) 2012-2015 Texas Instruments, Inc.
 *****************************************************************************/

package ti.instrumentation.wdtimer[1, 0, 0, 4] {
    module Settings;
    module WatchdogTimer;
}
